import React, {useState, useEffect} from "react";
import { Text, View, StyleSheet, TouchableOpacity, Dimensions } from "react-native";
import MapView, {Marker} from 'react-native-maps';
import colors from "../config/colors";
import useLocation from "../hooks/useLocation";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Screen from "../components/Screen";

function SelectMainLocation(){
    const location = useLocation();
    console.log('location ',location);
    const [pin, setPin] = useState(location);

    useEffect(() => {
        setPin(location);
        // setPin(pin)
    }, [location, pin]);

    return(
        <Screen >
        <View style={{marginTop: 100, flex: 1}}>
            { pin && 
                <>
                    <GooglePlacesAutocomplete
                        placeholder='Search'
                        // minLength={2} 
                        fetchDetails={true}
                        GooglePlacesSearchQuery={{
                            rankby: 'distance',
                        }}
                        onPress={(data, details = null) => {
                            // 'details' is provided when fetchDetails = true
                            console.log(data, details);
                            setPin({
                                latitude: details.geometry.location.lat,
                                longitude: details.geometry.location.lng,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            })
                        }}
                        query={{
                            key: 'AIzaSyApUI9ucN_LJNQyEq9fHfVjGhBA28imfPQ',
                            language: 'en',
                            components: 'country:au',
                       
                        }}
                        styles={{
                            container: { flex: 0, position: "absolute", width: "97%", zIndex: 1, margin: 5},
                            listView: { backgroundColor: "white" }
                        }}
                    />
                    <MapView 
                        style={styles.map} 
                        initialRegion={{
                            latitude: location.latitude,
                            longitude: location.longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                        provider="google">
                        <Marker coordinate={{ latitude: pin.latitude, longitude: pin.longitude }} />
                        
                    </MapView>
                </>
            }
            
        </View>
        </Screen>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.tertiary,
        alignItems: 'center',
        justifyContent: 'center',
        
      },
      map:{
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
      },
      

})

export default SelectMainLocation;