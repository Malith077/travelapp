import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import * as Yup from 'yup';
import { authentication } from '../config/firebaseConfig';
import { createUserWithEmailAndPassword } from 'firebase/auth';

import {
    AppForm,
    AppFormField,
    SubmitButton,
    ErrorMessage
} from '../components/forms'
import Screen from '../components/Screen';

const validationSchema = Yup.object().shape({
    name: Yup.string().required().label("Name"),
    email: Yup.string().required().label("Email"),
    password: Yup.string().required().min(4).label("Password")
});

function RegisterScreen(props) {
    const handleSubmit = async (userInfo) => {
        createUserWithEmailAndPassword(authentication, userInfo.email, userInfo.password).
        then((user) => {
            console.log('user', user);
        })
        .catch(error => {
            console.log(error);
        })
    }
    return(
        <Screen style={styles.container}>
            <AppForm 
                initialValues={{name: '', email: '', password: ''}}
                onSubmit={(values) => handleSubmit(values)}
                validationSchema={validationSchema}
            >
                <AppFormField 
                autoCapitalize="words"
                autoCorrect={false}
                icon="account"
                keyboardType="default"
                placeholder="Name"
                name="name"
                textContentType="name"
                />
                <AppFormField 
                    autoCapitalize="none"
                    autoCorrect={false}
                    icon="email"
                    keyboardType="email-address"
                    placeholder="Email"
                    name="email"
                    textContentType="emailAddress"
                />
                <AppFormField
                    autoCapitalize="none"
                    autoCorrect={false}
                    icon="lock"
                    placeholder="Password"
                    textContentType="password"
                    secureTextEntry
                    name="password"
                />
                <SubmitButton title="Register"/>
            </AppForm>
        </Screen>
    )
}
const styles = StyleSheet.create({
    container:{
        padding: 10
    }
})

export default RegisterScreen;