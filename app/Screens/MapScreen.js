import React, { useState, useEffect, createRef} from 'react'
import { Platform, Keyboard, Text } from 'react-native'
import axios from 'axios'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  View,
  Dimensions
} from 'react-native'
import Screen from '../components/Screen'
import useLocation from "../hooks/useLocation";
import MapView, { Marker, Callout } from 'react-native-maps';
import AppCallout from '../components/AppCallout'

const GOOGLE_PACES_API_BASE_URL = 'https://maps.googleapis.com/maps/api/place'
const API = 'AIzaSyApUI9ucN_LJNQyEq9fHfVjGhBA28imfPQ';

import SearchBarWithAutocomplete from '../components/SearchBarWithAutoComplete';

function MapScreen(){
  const [search, setSearch] = useState({ term: '', fetchPredictions: false});
  const [predictions, setPredictions] = useState([])
  const [showPredictions, setShowPredictions] = useState(false)
  const [resultDescription, setResultDescription] = useState('');
  const [selectCallout, setSelectCallout] = useState(false)
  const mapRef = createRef();
  const location = useLocation();
  const [pin, setPin] = useState(location);

  useEffect(() => {
      setPin(location);
      setResultDescription('You are here!')
  }, [location]);

  const onChangeText = async () => {
    if(search.term.length == 0 ) setPredictions([])
    if (search.term.trim() === '') return
    if (!search.fetchPredictions) return
    const apiUrl = `${GOOGLE_PACES_API_BASE_URL}/autocomplete/json?key=${API}&input=${search.term}&types=establishment&components=country:au`
    try {
      const result = await axios.request({
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        url: apiUrl,
        body: JSON.stringify({

        })
      })
      if (result) {
        const { data: { predictions } } = result
        setPredictions(predictions)
        setShowPredictions(true)
      }
    } catch (e) {
      console.log(e)
    }
  }

  const onPredictionTapped = async (placeId, description) => {
    const apiUrl = `${GOOGLE_PACES_API_BASE_URL}/details/json?key=${API}&place_id=${placeId}`
    try {
      const result = await axios.request({
        method: 'post',
        url: apiUrl
      })
      if (result) {
        const { data: { result: { geometry: { location } } } } = result
        const { lat, lng } = location
        setShowPredictions(false)
        setSearch({ term: description })
        setPin({
          latitude: lat,
          longitude: lng,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        })
        setResultDescription(description)
        setSelectCallout(true);
        console.log(description)
      }
    } catch (e) {
      console.log(e)
    }
  }

  const dragPin = (e) => {
    setPin(e.nativeEvent.coordinate);
    mapRef.current.onRegionChangeComplete({
      latitude: e.nativeEvent.coordinate.latitude,
      longitude: e.nativeEvent.coordinate.longitude,
      latitudeDelta: 0.1,
      longitudeDelta: 0.1,
    });
    Keyboard.dismiss();
  }

    return(
        <>
            <Screen style={styles.container}>
              <View style={styles.body}>
                <SearchBarWithAutocomplete
                  style={styles.search}
                  value={search.term}
                  onChangeText={(text) => {
                    setSearch({ term: text, fetchPredictions: true })
                    onChangeText()
                  }}
                  showPredictions={showPredictions}
                  predictions={predictions}
                  onPredictionTapped={onPredictionTapped}
                  resetValue={() => setSearch({ term: '', fetchPredictions: false })}
                />
              </View>
              { 
              pin &&
                <View style={styles.mapContainer}>
                  <MapView 
                    style={styles.map} 
                    initialRegion={{
                        latitude: location.latitude,
                        longitude: location.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                    ref={mapRef}
                    provider="google"
                    region={{
                      latitude: pin.latitude,
                      longitude: pin.longitude,
                      latitudeDelta: 0.0,
                      longitudeDelta: 0.009,
                    }}
                    >
                    <Marker 
                      draggable={true} 
                      coordinate={{latitude: pin.latitude, longitude: pin.longitude}} 
                      onDragEnd={(e) => dragPin(e)}
                    >
                      {
                        resultDescription.length > 0 ? 
                        // <Callout>
                        //   <Text>{resultDescription}</Text>
                        // </Callout>  
                        <AppCallout description={resultDescription} selectCallout={selectCallout} />
                        :
                        null
                      }
                      
                    </Marker>
                  </MapView>
                </View>
                }
            </Screen>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'flex-start',
      backgroundColor: '#ecf0f1',
    },
    body: {
      paddingHorizontal: 20,
      marginTop:20,
      zIndex: 10000,
      elevation: 1000
    },
    map:{
      position: 'absolute',
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    },
    search:{
      top: 100
    },
    mapContainer:{
      position: 'absolute',
    }
  })
  

export default MapScreen;