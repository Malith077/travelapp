import React from 'react';
import { Image, ImageBackground, StyleSheet , View, Text } from 'react-native';

// import routes from '../navigation/routes';
import AppButton from '../components/AppButton';

function WelcomeScreen({navigation}) {
    return (
        <ImageBackground
            blurRadius={5}
            style={styles.background}
            source={require('../assets/welcome_image.jpg')}>
            
            {/* <View style={styles.logoContainer}>
                <Image style={styles.logo} source={require('../assets/welcome_image.jpg')}/>
                <Text style={styles.tagline}>Sell what you don't need</Text>
            </View> */}

            <View style={styles.buttonsContainer}>
                <AppButton title="login" colour="tertiary" textColor="secondary" />
                <AppButton title="register" colour="secondary" />
            </View>
        </ImageBackground>
        
    );
}

const styles = StyleSheet.create({
    background:{
        flex: 1,
        flexDirection: "column",
        justifyContent: "flex-end",
        alignItems: "center"
    },
    logo:{
        width: 100,
        height: 100,
    },
    logoContainer:{
        position: "absolute",
        top: 70,
        alignItems: "center"
    },
    buttonsContainer:{
        padding: 20,
        width: "100%"
    },
    tagline:{
        fontSize: 25,
        fontWeight: "600",
        paddingVertical: 20
    }
})
export default WelcomeScreen; 