// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAI6L9BbsNsdwM2caQ89BlsPGozxkpNDOI",
  authDomain: "travelapp-73eae.firebaseapp.com",
  projectId: "travelapp-73eae",
  storageBucket: "travelapp-73eae.appspot.com",
  messagingSenderId: "135295542095",
  appId: "1:135295542095:web:f2beb78b5938fd279d9b3a",
  measurementId: "G-WDP7LL111D"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const authentication = getAuth(app);