import { Platform } from 'react-native'
import colours from './colors';

export default {
    colours,
    text:{
        fontSize: 20,
        fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
        color: colours.dark,
        elevation: 1000
    }
};