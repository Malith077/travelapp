export default {
    primary: '#00d278',
    secondary: '#333232',
    tertiary: '#f1f2ee',
    red: '#ff1d15',
    blue: '#5c7aff',
    white: '#fff',
}