import React from 'react';
import { TextInput, View, StyleSheet, Platform } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import defaultStyles from '../config/styles'
function AppTextInput({icon, width = "100%", ...otherProps}) {
    return (
        <View style={[styles.container, {width}]}>
            {icon && <MaterialCommunityIcons name={icon} size={20} color={defaultStyles.colours.medium} style={styles.icon} />}
            <TextInput 
                placeholderTextColor={defaultStyles.colours.medium}
                style={[defaultStyles.text, styles.text]} {...otherProps}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: defaultStyles.colours.light,
        borderRadius: 25,
        flexDirection: "row", 
        padding: 15,
        marginVertical: 10
    }, 
    icon:{
        margin:10
    },
    text: {
        width: "100%",
    }
})

export default AppTextInput;