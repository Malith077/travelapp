import React, {useState} from "react";
import { 
    TextInput, 
    View, 
    StyleSheet, 
    FlatList,
    TouchableOpacity,
    Text } from 'react-native'
import colors from "../config/colors";
import { MaterialCommunityIcons } from '@expo/vector-icons'

function SearchBarWithAutoComplete({
    value, 
    searchBarStyles, 
    onChangeText, 
    onPredictionTapped,
    predictions,
    showPredictions,
    resetValue
}) {

    const [inputSize, setInputSize] = useState({ width: 0, height: 0 })
    const inputBottomRadius = showPredictions ?
    {
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0
    }
    :
    {
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20
    }

    const _renderPredictions = (predictions) => {
        const {
          predictionsContainer,
          predictionRow
        } = styles
        const calculatedStyle = { 
          width: inputSize.width
        }
        
        return (
          <FlatList
            data={predictions}
            renderItem={({ item, index }) => {
              return (
                <TouchableOpacity
                  style={styles.predictionRow}
                  onPress={() => onPredictionTapped(item.place_id, item.description)}
                >
                  <Text
                    numberOfLines={1}
                  >
                    {item.description}
                  </Text>
                </TouchableOpacity>
              )
            }}
            keyExtractor={(item) => item.place_id}
            keyboardShouldPersistTaps='handled'
            style={[styles.predictionsContainer, calculatedStyle]}
          />
        )
      }

  return (
      <View style={[styles.container]}>
        <View style={styles.inputContainer}>
          <TextInput 
            style={styles.input}    
            placeholder="Where are you going to stay?"
            value={value}
            onChangeText={onChangeText}
            returnKeyType="search"
            onLayout={(event) => {
                const { height, width } = event.nativeEvent.layout
                setInputSize({ height, width })
              }}
          />
          {
            value.length > 0 ? 
            
            <TouchableOpacity
            style={styles.iconContainer}
            onPress={resetValue}
            >
              <MaterialCommunityIcons size={20} name="close-circle" color={colors.secondary} />
            </TouchableOpacity>
            :
            <>
            </>
          }
          
        </View>
        {showPredictions && _renderPredictions(predictions)}
      </View>
  )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
    },
    inputContainer :{
      paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: colors.tertiary,
        color: colors.secondary,
        fontSize: 18,
        borderRadius: 5,
        elevation: 1000,
        flexDirection: 'row',
        
    },
    iconContainer:{
      justifyContent: "center",
    alignItems: "center",
    },
    input: {
        // paddingVertical: 10,
        // paddingHorizontal: 10,
        // backgroundColor: colors.tertiary,
        color: colors.secondary,
        fontSize: 18,
        flex: 1,
        flexDirection: "column",

        // borderRadius: 5,
        // elevation: 1000
    },
    predictionRow: {
        paddingBottom: 15,
        marginBottom: 15,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        zIndex:1000,
        elevation:1000
    },
    predictionsContainer: {
        backgroundColor: colors.white,
        padding: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        elevation:1000
    },

})

export default SearchBarWithAutoComplete;