import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Callout }  from 'react-native-maps';
import colors from '../config/colors';

function AppCallout({ description, onPress, selectCallout }) {
    return (
            <Callout onPress={onPress} style={styles.callout} >
                <View style={styles.calloutContainer}>
                    <Text style={styles.calloutText} title={description}>{description}</Text>
                    {selectCallout && <Text style={styles.calloutTextBottom}>Choose here</Text>}
                </View>
            </Callout>
        )
}

const styles = StyleSheet.create({
    callout:{
        padding: 5,
    },
    calloutContainer: {
        borderRadius: 10,
        flex: 1,
        maxWidth: 200,
    },
    calloutText:{
        fontSize: 18,
        color:  colors.secondary,
    },
    calloutTextBottom:{
        fontSize: 12,
        textAlign: 'center',
    }

})

export default AppCallout;