import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { LogBox } from 'react-native'
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import MapView from 'react-native-maps';
import AppButton from './app/components/AppButton';
import AppText from './app/components/AppText';
import colors from './app/config/colors';
import SelectMainLocation from './app/Screens/SelectMainLocation';
import MapScreen from './app/Screens/MapScreen';
import WelcomeScreen from './app/Screens/WelcomeScreen';
import RegisterScreen from './app/Screens/Register';
export default function App() {
  LogBox.ignoreAllLogs();
  return (
    // <View style={styles.container}>
    //   <AppButton title="Test Button" />
    //   <AppText>Test Text</AppText>
    //   {/* <MapView style={styles.map} provider="google"/> */}
    //   <StatusBar style="auto" />
    // </View>
    // <WelcomeScreen />
    // <SelectMainLocation />
    <RegisterScreen />
    // <MapScreen />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.tertiary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  map:{
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  }
});
